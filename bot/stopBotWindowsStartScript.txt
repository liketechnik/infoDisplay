@if "%DEBUG%" == "" @echo off

@rem Create a file that signals the bot to stop and then remove it.

@rem path resolution is from gradles default windows start script
@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.\

set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%${appHomeRelativePath}

type NUL >> stop
ping -n 7 127.0.0.1 >nul
del stop
