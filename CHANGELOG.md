# Change Log

## [**Development**](https://github.com/liketechnik/infoDisplay/tree/HEAD)

[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/2.0.0...HEAD)

**Merged pull requests (development branch):**



## [2.0.0](https://github.com/liketechnik/infoDisplay/tree/2.0.0) (2017-05-26)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.2.3...2.0.0)

**Implemented enhancements:**

- Add internalisation [\#6](https://github.com/liketechnik/infoDisplay/issues/6)

**Fixed bugs:**

- Danger of hitting telegram limits [\#14](https://github.com/liketechnik/infoDisplay/issues/14)

**Closed issues:**

- vulnerabilities in logback dependency [\#30](https://github.com/liketechnik/infoDisplay/issues/30)
- Test ticket from Code Climate [\#27](https://github.com/liketechnik/infoDisplay/issues/27)
- Test ticket from Code Climate [\#26](https://github.com/liketechnik/infoDisplay/issues/26)
- Option to list images, rearrange them and delete them [\#11](https://github.com/liketechnik/infoDisplay/issues/11)
- Send and display videos [\#10](https://github.com/liketechnik/infoDisplay/issues/10)

**Merged pull requests (development branch):**

- delete files [\#36](https://github.com/liketechnik/infoDisplay/pull/36) ([liketechnik](https://github.com/liketechnik))
- send help menu when receiving unknown commands / text / documents [\#35](https://github.com/liketechnik/infoDisplay/pull/35) ([liketechnik](https://github.com/liketechnik))
- added threading model [\#34](https://github.com/liketechnik/infoDisplay/pull/34) ([liketechnik](https://github.com/liketechnik))
- Add options to change the language of the bot [\#33](https://github.com/liketechnik/infoDisplay/pull/33) ([liketechnik](https://github.com/liketechnik))
- Changed dependency for QOS.ch Logback library to version 1.1.11 [\#32](https://github.com/liketechnik/infoDisplay/pull/32) ([liketechnik](https://github.com/liketechnik))
- Changed dependency for QOS.ch Logback library to version 1.1.11 [\#31](https://github.com/liketechnik/infoDisplay/pull/31) ([liketechnik](https://github.com/liketechnik))
- travis ci integration [\#29](https://github.com/liketechnik/infoDisplay/pull/29) ([liketechnik](https://github.com/liketechnik))
- send\_video\#10 - Send videos via bot [\#28](https://github.com/liketechnik/infoDisplay/pull/28) ([liketechnik](https://github.com/liketechnik))

## [1.2.3](https://github.com/liketechnik/infoDisplay/tree/1.2.3) (2017-03-19)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.2.2...1.2.3)

## [1.2.2](https://github.com/liketechnik/infoDisplay/tree/1.2.2) (2017-03-07)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.2.1...1.2.2)

**Merged pull requests (development branch):**

- Different loading of message strings [\#25](https://github.com/liketechnik/infoDisplay/pull/25) ([liketechnik](https://github.com/liketechnik))
- Create LICENSE.txt [\#24](https://github.com/liketechnik/infoDisplay/pull/24) ([liketechnik](https://github.com/liketechnik))
- Create LICENSE.txt [\#23](https://github.com/liketechnik/infoDisplay/pull/23) ([liketechnik](https://github.com/liketechnik))

## [1.2.1](https://github.com/liketechnik/infoDisplay/tree/1.2.1) (2017-01-20)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.2...1.2.1)

**Merged pull requests (development branch):**

- video [\#22](https://github.com/liketechnik/infoDisplay/pull/22) ([liketechnik](https://github.com/liketechnik))
- Release version 1.2 [\#21](https://github.com/liketechnik/infoDisplay/pull/21) ([liketechnik](https://github.com/liketechnik))
- Release version 1.2 [\#20](https://github.com/liketechnik/infoDisplay/pull/20) ([liketechnik](https://github.com/liketechnik))

## [1.2](https://github.com/liketechnik/infoDisplay/tree/1.2) (2017-01-19)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.9...1.2)

**Implemented enhancements:**

- Add /help to the end of all messages from finished commands [\#8](https://github.com/liketechnik/infoDisplay/issues/8)

## [1.1.9](https://github.com/liketechnik/infoDisplay/tree/1.1.9) (2017-01-13)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.8...1.1.9)

**Closed issues:**

- Add about command... [\#9](https://github.com/liketechnik/infoDisplay/issues/9)

**Merged pull requests (development branch):**

- add "/help" at end of commands [\#19](https://github.com/liketechnik/infoDisplay/pull/19) ([liketechnik](https://github.com/liketechnik))
- Changed directory structure and gradle build structure [\#18](https://github.com/liketechnik/infoDisplay/pull/18) ([liketechnik](https://github.com/liketechnik))

## [1.1.8](https://github.com/liketechnik/infoDisplay/tree/1.1.8) (2016-12-19)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.7...1.1.8)

**Implemented enhancements:**

- Command descriptions [\#7](https://github.com/liketechnik/infoDisplay/issues/7)

**Merged pull requests (development branch):**

- Add about command [\#17](https://github.com/liketechnik/infoDisplay/pull/17) ([liketechnik](https://github.com/liketechnik))

## [1.1.7](https://github.com/liketechnik/infoDisplay/tree/1.1.7) (2016-12-19)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.6...1.1.7)

**Merged pull requests (development branch):**

- Command descriptions [\#16](https://github.com/liketechnik/infoDisplay/pull/16) ([liketechnik](https://github.com/liketechnik))

## [1.1.6](https://github.com/liketechnik/infoDisplay/tree/1.1.6) (2016-12-19)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.5...1.1.6)

**Closed issues:**

- Test ticket from Code Climate [\#13](https://github.com/liketechnik/infoDisplay/issues/13)

**Merged pull requests (development branch):**

- Internationalization [\#15](https://github.com/liketechnik/infoDisplay/pull/15) ([liketechnik](https://github.com/liketechnik))

## [1.1.5](https://github.com/liketechnik/infoDisplay/tree/1.1.5) (2016-11-28)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.4...1.1.5)

**Implemented enhancements:**

- Allow modificaton of bot name and token without recompiling [\#3](https://github.com/liketechnik/infoDisplay/issues/3)

**Fixed bugs:**

- Allow uploading of uncompressed pictures [\#4](https://github.com/liketechnik/infoDisplay/issues/4)

**Closed issues:**

- Rewrite code of Display.java and add documentation to it. [\#2](https://github.com/liketechnik/infoDisplay/issues/2)
- Add documentation \(java doc\) to all classes and methods [\#1](https://github.com/liketechnik/infoDisplay/issues/1)

**Merged pull requests (development branch):**

- source-clear [\#12](https://github.com/liketechnik/infoDisplay/pull/12) ([liketechnik](https://github.com/liketechnik))

## [1.1.4](https://github.com/liketechnik/infoDisplay/tree/1.1.4) (2016-11-21)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.3...1.1.4)

## [1.1.3](https://github.com/liketechnik/infoDisplay/tree/1.1.3) (2016-11-20)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.2...1.1.3)

## [1.1.2](https://github.com/liketechnik/infoDisplay/tree/1.1.2) (2016-11-08)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.1...1.1.2)

## [1.1.1](https://github.com/liketechnik/infoDisplay/tree/1.1.1) (2016-11-07)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.1.0...1.1.1)

## [1.1.0](https://github.com/liketechnik/infoDisplay/tree/1.1.0) (2016-11-07)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.0.2...1.1.0)

## [1.0.2](https://github.com/liketechnik/infoDisplay/tree/1.0.2) (2016-11-07)
[Full Changelog](https://github.com/liketechnik/infoDisplay/compare/1.0.1...1.0.2)

## [1.0.1](https://github.com/liketechnik/infoDisplay/tree/1.0.1) (2016-11-06)


\* *This Change Log was automatically generated by [github_changelog_generator](https://github.com/skywinder/Github-Changelog-Generator)*