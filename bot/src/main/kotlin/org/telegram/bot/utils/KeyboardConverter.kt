/*
 * Copyright (C) 2016-2017  Florian Warzecha <flowa2000@gmail.com>
 *
 * This file is part of infoDisplay.
 *
 * infoDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * infoDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * infoDisplay uses TelegramBots Java API <https://github.com/rubenlagus/TelegramBots> by Ruben Bermudez.
 * TelegramBots API is licensed under GNU General Public License version 3 <https://www.gnu.org/licenses/gpl-3.0.de.html>.
 *
 * infoDisplay uses parts of the Apache Commons project <https://commons.apache.org/>.
 * Apache commons is licensed under the Apache License Version 2.0 <http://www.apache.org/licenses/>.
 *
 * infoDisplay uses vlcj library <http://capricasoftware.co.uk/#/projects/vlcj>.
 * vlcj is licensed under GNU General Public License version 3 <https://www.gnu.org/licenses/gpl-3.0.de.html>.
 *
 * Thanks to all the people who contributed to the projects that make this
 * program possible.
 */

package org.telegram.bot.utils

import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton

/**
 * A method that simplifies modifying of the callback data of [InlineKeyboardButton]s with non static content via [HashMap]s.
 *
 * It works by iterating over all the [InlineKeyboardButton]s of the keyboard and check if one of the keys in the map
 * match text in the button. This text is then replaced by the corresponding value of the key.
 * @author Florian Warzecha
 * @since 2.0.0
 * @version 1.0
 * @date 21 of Mai 2017
 */
internal fun convertDeleteMediaKeyboard(regexReplacementMap: HashMap<String, Int>, keyboard: List<List<InlineKeyboardButton>>):
        List<List<InlineKeyboardButton>> {
    for (inlineKeyboardButtons in keyboard) {
        for (button in inlineKeyboardButtons) {
            for (regexp in regexReplacementMap.keys) {
                button.callbackData = button.callbackData.replace(regexp, regexReplacementMap[regexp].toString())
            }
        }
    }
    return keyboard
}
